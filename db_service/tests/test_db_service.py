import base64
import random
import subprocess
import uuid

import pytest
import time
from fastapi import status, APIRouter
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import HTTPException
from fastapi.testclient import TestClient

from src.database_service.database_service import DatabaseService
from src.database_service.database_service_interface import DatabaseServiceInterface
from src.meme_repo.meme_repository_interface import MemeRepositoryInterface
from src.meme_repo.sqlalchemy_meme_repo import MemeRepository
from src.models import CreateMemeRequest, UpdateMemeRequest
from src.postgres.sqlalchemy_connection import PostgresConnection
from src.router import get_router

_fixed_seed_random = random.Random(1)


@pytest.fixture(scope="function")
def db_connection() -> PostgresConnection:
    # it would've been better to set up the database and run the tests in docker
    subprocess.run(
        args=['docker-compose', '-f', 'docker-compose.test.yml', 'up', '-d'],
    )

    for _ in range(30):
        time.sleep(1)
        if subprocess.run(
            args=['pg_isready', '--host=127.0.0.1', '--port=2345'],
            stdout=subprocess.PIPE,
        ).stdout.decode('utf-8').strip()[-11:] != 'no response':
            break
    else:
        pytest.fail("Couldn't connect to the database")

    connection = PostgresConnection(
        db_url=f"postgresql://test_user:test_password@127.0.0.1:2345/test_db",
    )

    yield connection

    subprocess.run(
        args=['docker-compose', '-f', 'docker-compose.test.yml', 'down'],
    )


@pytest.fixture(scope="function")
def meme_repository(db_connection: PostgresConnection) -> MemeRepositoryInterface:
    meme_repo = MemeRepository(
        db_connection=db_connection,
    )

    yield meme_repo


@pytest.fixture(scope='function')
def database_service(meme_repository: MemeRepositoryInterface) -> DatabaseServiceInterface:
    service: DatabaseServiceInterface = DatabaseService(
        meme_repository=meme_repository,
    )

    yield service


@pytest.fixture(scope='function')
def router(database_service: DatabaseServiceInterface) -> APIRouter:
    _router = get_router(database_service)

    yield _router


@pytest.fixture(scope='function')
def client(router: APIRouter) -> TestClient:
    _client = TestClient(router)

    yield _client

    _client.close()


def get_random_b64_string() -> str:
    return base64.b64encode(_fixed_seed_random.randbytes(100)).decode()


def get_create_meme_request() -> CreateMemeRequest:
    meme_caption: str = get_random_b64_string()

    meme_request = CreateMemeRequest(
        meme_id=str(uuid.uuid4()),
        image_id=str(uuid.uuid4()),
        caption=meme_caption
    )

    return meme_request


class TestDatabaseService:
    ROUTE_PREFIX = "/memes"

    def create_memes(self, client, requests: list[CreateMemeRequest]):
        for request in requests:
            response = client.post(
                f"{self.ROUTE_PREFIX}/",
                json=jsonable_encoder(request)
            )
            assert response.status_code == status.HTTP_201_CREATED

    def test_create_meme(self, client):
        create_meme1_request = get_create_meme_request()
        create_meme2_request = get_create_meme_request()

        create_meme2_request.caption = None
        self.create_memes(
            client,
            [create_meme1_request, create_meme2_request]
        )

        response1 = client.get(
            f"{self.ROUTE_PREFIX}/{create_meme1_request.meme_id}"
        )
        assert response1.status_code == status.HTTP_200_OK
        assert response1.json()["meme_id"] == create_meme1_request.meme_id
        assert response1.json()["image_id"] == create_meme1_request.image_id
        assert response1.json()["caption"] == create_meme1_request.caption

        response2 = client.get(
            f"{self.ROUTE_PREFIX}/{create_meme2_request.meme_id}"
        )
        assert response2.status_code == status.HTTP_200_OK
        assert response2.json()["meme_id"] == create_meme2_request.meme_id
        assert response2.json()["image_id"] == create_meme2_request.image_id
        assert response2.json()["caption"] is None

    def test_retrieve_memes_paginated(self, client):
        meme1_request = get_create_meme_request()
        meme2_request = get_create_meme_request()
        meme3_request = get_create_meme_request()

        meme_requests = [meme1_request, meme2_request, meme3_request]
        self.create_memes(
            client,
            meme_requests
        )

        skip = 0
        limit = 3
        all_memes_response = client.get(
            f"{self.ROUTE_PREFIX}/?skip={skip}&limit={limit}",
        )
        assert all_memes_response.status_code == status.HTTP_200_OK
        assert len(all_memes_response.json()) == 3

        all_memes_response_json: list[dict[str, str]] = all_memes_response.json()
        meme_request: CreateMemeRequest
        for meme_request, meme_response in zip(meme_requests, all_memes_response_json):
            assert meme_request.meme_id == meme_response["meme_id"]
            assert meme_request.image_id == meme_response["image_id"]
            assert meme_request.caption == meme_response["caption"]

        skip = 1
        limit = 3
        all_memes_response = client.get(
            f"{self.ROUTE_PREFIX}/?skip={skip}&limit={limit}",
        )
        assert all_memes_response.status_code == status.HTTP_200_OK
        assert len(all_memes_response.json()) == 2

        meme_requests = meme_requests[1:]  # skip first meme
        all_memes_response_json: list[dict[str, str]] = all_memes_response.json()
        meme_request: CreateMemeRequest
        for meme_request, meme_response in zip(meme_requests, all_memes_response_json):
            assert meme_request.meme_id == meme_response["meme_id"]
            assert meme_request.image_id == meme_response["image_id"]
            assert meme_request.caption == meme_response["caption"]

    def test_update_meme(self, client):
        meme1_request = get_create_meme_request()
        meme2_request = get_create_meme_request()

        meme_requests = [meme1_request, meme2_request]

        self.create_memes(
            client,
            meme_requests
        )

        meme2_updated_caption = "this caption was updated. nothing to see here"
        update_meme2_request = UpdateMemeRequest(
            caption=meme2_updated_caption
        )
        update_meme2_response = client.put(
            f"{self.ROUTE_PREFIX}/{meme2_request.meme_id}",
            json=jsonable_encoder(update_meme2_request)
        )
        assert update_meme2_response.status_code == status.HTTP_200_OK

        retrieve_meme2_response = client.get(
            f"{self.ROUTE_PREFIX}/{meme2_request.meme_id}"
        )
        assert retrieve_meme2_response.status_code == status.HTTP_200_OK
        assert retrieve_meme2_response.json()["meme_id"] == meme2_request.meme_id
        assert retrieve_meme2_response.json()["image_id"] == meme2_request.image_id
        assert retrieve_meme2_response.json()["caption"] == meme2_updated_caption

        retrieve_meme1_response = client.get(
            f"{self.ROUTE_PREFIX}/{meme1_request.meme_id}"
        )
        assert retrieve_meme1_response.status_code == status.HTTP_200_OK
        assert retrieve_meme1_response.json()["meme_id"] == meme1_request.meme_id
        assert retrieve_meme1_response.json()["image_id"] == meme1_request.image_id
        assert retrieve_meme1_response.json()["caption"] == meme1_request.caption

        update_meme1_request = UpdateMemeRequest()
        with pytest.raises(HTTPException):
            update_meme1_response = client.put(
                f"{self.ROUTE_PREFIX}/{meme1_request.meme_id}",
                json=jsonable_encoder(update_meme1_request)
            )
            assert update_meme1_response.status_code == status.HTTP_400_BAD_REQUEST
            assert update_meme1_response.json() == {}

        create_meme3_request = get_create_meme_request()
        with pytest.raises(HTTPException):
            update_meme3_response = client.put(
                f"{self.ROUTE_PREFIX}/very-fake-id",
                json=jsonable_encoder(create_meme3_request)
            )
            assert update_meme3_response.status_code == status.HTTP_404_NOT_FOUND

    def test_delete_meme(self, client):
        meme1_request = get_create_meme_request()
        meme2_request = get_create_meme_request()

        meme_requests = [meme1_request, meme2_request]

        self.create_memes(
            client,
            meme_requests
        )

        delete_meme2_response = client.delete(
            f"{self.ROUTE_PREFIX}/{meme2_request.meme_id}"
        )
        assert delete_meme2_response.status_code == status.HTTP_200_OK

        with pytest.raises(HTTPException):
            retrieve_meme2_response = client.get(
                f"{self.ROUTE_PREFIX}/{meme2_request.meme_id}"
            )
            assert retrieve_meme2_response.status_code == status.HTTP_404_NOT_FOUND

        retrieve_meme1_response = client.get(
            f"{self.ROUTE_PREFIX}/{meme1_request.meme_id}"
        )
        assert retrieve_meme1_response.status_code == status.HTTP_200_OK
        assert retrieve_meme1_response.json()["meme_id"] == meme1_request.meme_id
        assert retrieve_meme1_response.json()["image_id"] == meme1_request.image_id
        assert retrieve_meme1_response.json()["caption"] == meme1_request.caption
