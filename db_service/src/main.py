import logging
import os

from dotenv import load_dotenv
from fastapi import FastAPI

from src.database_service.database_service import DatabaseService
from src.database_service.database_service_interface import DatabaseServiceInterface
from src.meme_repo.meme_repository_interface import MemeRepositoryInterface
from src.meme_repo.sqlalchemy_meme_repo import MemeRepository
from src.postgres.sqlalchemy_connection import PostgresConnection
from src.router import get_router

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO,
    force=True
)
logger = logging.getLogger(__name__)


if os.path.isfile("src/env"):
    load_dotenv(
        dotenv_path="src/env",
        override=False,
    )
    logger.info("Loaded environment variables from .env file")
elif os.path.isfile("src/.env.example"):
    load_dotenv(
        dotenv_path="src/.env.example",
        override=False,
    )
    logger.info("Loaded default environment variables from .env.example file")
else:
    logger.warning("Couldn't find .env or .env.example file in current directory")

log_level = os.getenv('LOG_LEVEL', 'INFO')
logger.setLevel(log_level)

app = FastAPI()
logger.info("Initialized FastAPI app")

assert "DB_URL" in os.environ, "DB_URL is not set"
db_url = os.environ["DB_URL"]
db_connection = PostgresConnection(
    db_url=db_url,
)
logger.info("Database connection established")

# TODO: implement
meme_repository: MemeRepositoryInterface = MemeRepository(
    db_connection=db_connection,
)
logger.info("Database repository initialized")

db_service: DatabaseServiceInterface = DatabaseService(
    meme_repository=meme_repository,
)
logger.info("Database service initialized")

router = get_router(db_service)
logger.info("Database router initialized")

app.include_router(router)
logger.info("Database router included, starting up..")
