from pydantic import BaseModel
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import DeclarativeBase


class Base(DeclarativeBase):
    pass


class Meme(Base):
    __tablename__ = "memes"

    # private id
    id = Column(Integer, primary_key=True)

    # public id
    unique_meme_id = Column(
        String,
        unique=True,
        index=True,
    )
    unique_image_id = Column(
        String,
        unique=True,
        index=True,
    )

    caption = Column(String, nullable=True)


class MemeUpdate(BaseModel):
    unique_meme_id: str | None
    unique_image_id: str | None
    caption: str | None


class CreateMemeRequest(BaseModel):
    meme_id: str
    image_id: str
    caption: str | None = None

    def to_model(self):
        return Meme(
            unique_meme_id=self.meme_id,
            unique_image_id=self.image_id,
            caption=self.caption,
        )


class RetrieveMemeResponse(BaseModel):
    meme_id: str
    image_id: str
    caption: str | None = None


class UpdateMemeRequest(BaseModel):
    meme_id: str | None = None
    image_id: str | None = None
    caption: str | None = None

    def to_model(self, meme_id: str):
        return Meme(
            unique_meme_id=meme_id,
            unique_image_id=self.image_id,
            caption=self.caption,
        )
