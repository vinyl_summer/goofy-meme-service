import logging
import os

from dotenv import load_dotenv
from fastapi import FastAPI

from src.db_service_client.db_service_client import DatabaseServiceClient
from src.db_service_client.db_service_client_interface import DatabaseServiceClientInterface
from src.image_service_client.image_service_client import ImageServiceClient
from src.image_service_client.image_service_client_interface import ImageServiceClientInterface
from src.meme_service.meme_service import MemeService
from src.meme_service.meme_service_interface import MemeServiceInterface
from src.router import get_router

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO,
    force=True
)
logger = logging.getLogger(__name__)


if os.path.isfile("src/.env"):
    load_dotenv(
        dotenv_path="src/.env",
        override=False,
    )
    logger.info("Loaded environment variables from .env file")
elif os.path.isfile("src/.env.example"):
    load_dotenv(
        dotenv_path="src/.env.example",
        override=False,
    )
    logger.info("Loaded default environment variables from .env.example file")
else:
    logger.warning("Couldn't find .env or .env.example file in current directory")

log_level = os.getenv('LOG_LEVEL', 'INFO')
logger.setLevel(log_level)


app = FastAPI()
logger.info("Successfully initialized FastAPI app")

assert "IMAGE_SERVICE_ENDPOINT" in os.environ, "IMAGE_SERVICE_ENDPOINT environment variable must be set"

image_service_endpoint = os.environ["IMAGE_SERVICE_ENDPOINT"]
image_service_client: ImageServiceClientInterface = ImageServiceClient(
    image_service_endpoint=image_service_endpoint,
)
logger.info("Successfully initialized image service client")

assert "DB_SERVICE_ENDPOINT" in os.environ, "DB_SERVICE_ENDPOINT environment variable must be set"

database_service_endpoint = os.environ["DB_SERVICE_ENDPOINT"]
database_service_client: DatabaseServiceClientInterface = DatabaseServiceClient(
    db_service_endpoint=database_service_endpoint,
)
logger.info("Successfully initialized database service client")

service: MemeServiceInterface = MemeService(
    image_service_client=image_service_client,
    db_service_client=database_service_client,
)
logger.info("Successfully initialized Meme Service")

meme_router = get_router(service)

app.include_router(meme_router)
logger.info("Successfully included meme service API router, starting up..")
