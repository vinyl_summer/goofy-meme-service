import logging
import os

from dotenv import load_dotenv
from fastapi import FastAPI

from src.image_service.image_service import ImageService
from src.router import get_router
from src.storage_service_client.storage_service_client import StorageServiceClient
from src.storage_service_client.storage_service_client_interface import StorageServiceClientInterface

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO,
    force=True
)
logger = logging.getLogger(__name__)


if os.path.isfile("src/.env"):
    load_dotenv(
        dotenv_path="src/.env",
        override=False,
    )
    logger.info("Loaded environment variables from .env file")
elif os.path.isfile("src/.env.example"):
    load_dotenv(
        dotenv_path="src/.env.example",
        override=False,
    )
    logger.info("Loaded default environment variables from .env.example file")
else:
    logger.warning("Couldn't find .env or .env.example file in current directory")

log_level = os.getenv('LOG_LEVEL', 'INFO')
logger.setLevel(log_level)

app = FastAPI()
logger.info("Successfully initialized FastAPI app")

assert "S3_ENDPOINT" in os.environ, "S3_ENDPOINT environment variable must be set"
storage_service_endpoint: str = os.getenv("S3_ENDPOINT")
storage_service_client: StorageServiceClientInterface = StorageServiceClient(
    storage_service_endpoint=storage_service_endpoint,
)
logger.info("Successfully initialized S3 client")

service = ImageService(
    storage_service_client=storage_service_client,
)
logger.info("Successfully initialized image service")

image_router = get_router(
    image_service=service,
)
logger.info("Successfully initialized image router")

app.include_router(image_router)
logger.info("Successfully included image service router, starting up..")
