import uuid

from pydantic import BaseModel, Field


class Image(BaseModel):
    image_id: str = Field(default_factory=lambda: str(uuid.uuid4()))
    b64_data: str


class CreateImageRequest(BaseModel):
    b64_data: str

    def to_model(self) -> Image:
        return Image(
            b64_data=self.b64_data
        )


class CreateImageResponse(BaseModel):
    image_id: str


class GetImageResponse(BaseModel):
    b64_data: str


class UpdateImageRequest(BaseModel):
    b64_data: str

    def to_model(self, image_id: str) -> Image:
        return Image(
            image_id=image_id,
            b64_data=self.b64_data
        )
