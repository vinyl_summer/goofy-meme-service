import base64
import random

import pytest
from fastapi import APIRouter, status, HTTPException
from fastapi.encoders import jsonable_encoder
from fastapi.testclient import TestClient

from src.image_service.image_service import ImageService
from src.image_service.image_service_interface import ImageServiceInterface
from src.models import CreateImageRequest
from src.router import get_router
from src.storage_service_client.fake_storage_service_client import FakeStorageServiceClient

_fixed_seed_random = random.Random(1)


@pytest.fixture(scope="function")
def storage_service_client():
    client = FakeStorageServiceClient()

    yield client


@pytest.fixture(scope="function")
def image_service(storage_service_client):
    service: ImageServiceInterface = ImageService(
        storage_service_client=storage_service_client
    )

    yield service


@pytest.fixture(scope='function')
def router(image_service) -> APIRouter:
    _router = get_router(image_service)

    yield _router


@pytest.fixture(scope='function')
def client(router) -> TestClient:
    _client = TestClient(router)

    yield _client

    _client.close()


def get_random_b64_string() -> str:
    return base64.b64encode(_fixed_seed_random.randbytes(100)).decode()


def get_create_image_request() -> CreateImageRequest:
    image_b64_data: str = get_random_b64_string()

    create_image_request = CreateImageRequest(
        b64_data=image_b64_data,
    )

    return create_image_request


class TestImageService:
    ROUTE_PREFIX = "/images"

    def create_images(self, test_client: TestClient, requests: list[CreateImageRequest]) -> list[str]:
        created_image_ids: list[str] = []
        for request in requests:
            response = test_client.post(
                f"{self.ROUTE_PREFIX}/",
                json=jsonable_encoder(request)
            )
            assert response.status_code == status.HTTP_201_CREATED
            created_image_ids.append(response.json().get("image_id"))

        return created_image_ids

    def test_create_image(self, client):
        image1_request = get_create_image_request()
        image2_request = get_create_image_request()

        image1_id, image2_id = self.create_images(client, [image1_request, image2_request])

        response1 = client.get(
            f"{self.ROUTE_PREFIX}/{image1_id}",
        )
        assert response1.status_code == status.HTTP_200_OK
        assert response1.json()["b64_data"] == image1_request.b64_data

        response2 = client.get(
            f"{self.ROUTE_PREFIX}/{image2_id}"
        )
        assert response2.status_code == status.HTTP_200_OK
        assert response2.json()["b64_data"] == image2_request.b64_data

        with pytest.raises(HTTPException):
            response3 = client.get(
                f"{self.ROUTE_PREFIX}/idk",
            )
            assert response3.status_code == status.HTTP_404_NOT_FOUND

    def test_update_image(self, client):
        image1_request = get_create_image_request()
        image2_request = get_create_image_request()

        image1_id, image2_id = self.create_images(client, [image1_request, image2_request])

        image3_request = get_create_image_request()

        update_image1_response = client.put(
            f"{self.ROUTE_PREFIX}/{image1_id}",
            json={"b64_data": image3_request.b64_data},
        )
        assert update_image1_response.status_code == status.HTTP_200_OK

        retrieve_image1_response = client.get(
            self.ROUTE_PREFIX + f"/{image1_id}"
        )
        assert retrieve_image1_response.status_code == status.HTTP_200_OK
        assert retrieve_image1_response.json()["b64_data"] == image3_request.b64_data

        retrieve_image2_response = client.get(
            f"{self.ROUTE_PREFIX}/{image2_id}"
        )
        assert retrieve_image2_response.status_code == status.HTTP_200_OK
        assert retrieve_image2_response.json()["b64_data"] == image2_request.b64_data

    def test_delete_image(self, client):
        image1_request = get_create_image_request()
        image2_request = get_create_image_request()

        image1_id, image2_id = self.create_images(client, [image1_request, image2_request])

        delete_image1_response = client.delete(
            f"{self.ROUTE_PREFIX}/{image1_id}"
        )
        assert delete_image1_response.status_code == status.HTTP_200_OK

        with pytest.raises(HTTPException):
            retrieve_image1_response = client.get(
                f"{self.ROUTE_PREFIX}/{image1_id}"
            )
            assert retrieve_image1_response.status_code == status.HTTP_404_NOT_FOUND

        retrieve_image2_response = client.get(
            f"{self.ROUTE_PREFIX}/{image2_id}"
        )
        assert retrieve_image2_response.status_code == status.HTTP_200_OK
        assert retrieve_image2_response.json()["b64_data"] == image2_request.b64_data
