import base64
import random
import subprocess
import uuid

import pytest
from fastapi import APIRouter, status, HTTPException
from fastapi.encoders import jsonable_encoder
from fastapi.testclient import TestClient

from src.minio_client import MinIOClient
from src.models import CreateDataRequest
from src.router import get_router
from src.storage_service.storage_service import StorageService
from src.storage_service.storage_service_interface import StorageServiceInterface

_fixed_seed_random = random.Random(1)


@pytest.fixture(scope='function')
def minio_client() -> MinIOClient:
    minio_root_user = 'minio_test'
    minio_root_password = 'minio_test'
    subprocess.run(
        args=['docker-compose', '-f', 'docker-compose.test.yml', 'up', '-d'],
        env={
            'MINIO_ROOT_USER': minio_root_user,
            'MINIO_ROOT_PASSWORD': minio_root_password,
        },
    )

    minio_bucket = 'test-bucket'
    client = MinIOClient(
        minio_endpoint='127.0.0.1:9000',
        minio_access_key=minio_root_user,
        minio_secret_key=minio_root_password,
        minio_bucket=minio_bucket,
        secure=False
    )

    yield client

    subprocess.run(
        args=['docker-compose', '-f', 'docker-compose.test.yml', 'down'],
    )


@pytest.fixture(scope='function')
def storage_service(minio_client: MinIOClient) -> StorageServiceInterface:
    service: StorageServiceInterface = StorageService(
        storage_service_client=minio_client,
    )

    yield service


@pytest.fixture(scope='function')
def router(storage_service) -> APIRouter:
    _router = get_router(storage_service)

    yield _router


@pytest.fixture(scope='function')
def client(router) -> TestClient:
    _client = TestClient(router)

    yield _client

    _client.close()


def get_random_b64_string() -> str:
    return base64.b64encode(_fixed_seed_random.randbytes(100)).decode()


def get_create_meme_request() -> CreateDataRequest:
    key = str(uuid.uuid4())
    b64_data = get_random_b64_string()

    data_request = CreateDataRequest(
        key=key,
        b64_data=b64_data,
    )

    return data_request


class TestStorageService:
    ROUTE_PREFIX = "/data"

    def create_data(self, test_client, requests: list[CreateDataRequest]):
        for request in requests:
            response = test_client.post(
                f"{self.ROUTE_PREFIX}/",
                json=jsonable_encoder(request)
            )
            assert response.status_code == status.HTTP_201_CREATED

    def test_data_upload(self, client):
        create_data1_request = get_create_meme_request()
        create_data2_request = get_create_meme_request()

        self.create_data(client, [create_data1_request, create_data2_request])

        response1 = client.get(
            f"{self.ROUTE_PREFIX}/{create_data1_request.key}"
        )
        assert response1.status_code == status.HTTP_200_OK
        assert response1.json()['b64_data'] == create_data1_request.b64_data

        response2 = client.get(
            f"{self.ROUTE_PREFIX}/{create_data2_request.key}"
        )
        assert response2.status_code == status.HTTP_200_OK
        assert response2.json()['b64_data'] == create_data2_request.b64_data

    def test_data_deletion(self, client):
        create_data1_request = get_create_meme_request()
        create_data2_request = get_create_meme_request()

        self.create_data(client, [create_data1_request, create_data2_request])

        delete_data1_response = client.delete(
            f"{self.ROUTE_PREFIX}/{create_data1_request.key}"
        )
        assert delete_data1_response.status_code == status.HTTP_200_OK

        with pytest.raises(HTTPException):
            delete_data1_response = client.delete(
                f"{self.ROUTE_PREFIX}/{create_data1_request.key}"
            )
            assert delete_data1_response.status_code == status.HTTP_404_NOT_FOUND

        with pytest.raises(HTTPException):
            retrieve_data1_response = client.get(
                f"{self.ROUTE_PREFIX}/{create_data1_request.key}"
            )
            assert retrieve_data1_response.status_code == status.HTTP_404_NOT_FOUND

        retrieve_data2_response = client.get(
            f"{self.ROUTE_PREFIX}/{create_data2_request.key}"
        )
        assert retrieve_data2_response.status_code == status.HTTP_200_OK
        assert retrieve_data2_response.json()['b64_data'] == create_data2_request.b64_data
