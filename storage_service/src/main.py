import logging
import os

from dotenv import load_dotenv
from fastapi import FastAPI

from src.minio_client import MinIOClient
from src.router import get_router
from src.storage_service.storage_service import StorageService
from src.storage_service.storage_service_interface import StorageServiceInterface

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO,
    force=True
)
logger = logging.getLogger(__name__)

if os.path.isfile("src/.env"):
    load_dotenv(
        dotenv_path="src/.env",
        override=False,
    )
    logger.info("Loaded environment variables from .env file")
elif os.path.isfile("src/.env.example"):
    load_dotenv(
        dotenv_path="src/.env.example",
        override=False,
    )
    logger.info("Loaded default environment variables from .env.example file")
else:
    logger.warning("Couldn't find .env or .env.example file in current directory")

log_level = os.getenv('LOG_LEVEL', 'INFO')
logger.setLevel(log_level)


app = FastAPI()
logger.info("Initialized FastAPI app")

assert "S3_ENDPOINT" in os.environ, "S3_ENDPOINT environment variable must be set"
s3_endpoint = os.environ['S3_ENDPOINT']
assert "S3_ACCESS_KEY" in os.environ, "S3_ACCESS_KEY environment variable must be set"
s3_access_key = os.environ['S3_ACCESS_KEY']
assert "S3_SECRET_KEY" in os.environ, "S3_SECRET_KEY environment variable must be set"
s3_secret_key = os.environ['S3_SECRET_KEY']
assert "S3_BUCKET" in os.environ, "S3_BUCKET environment variable must be set"
s3_bucket = os.environ['S3_BUCKET']

storage_service_client = MinIOClient(
    minio_endpoint=s3_endpoint,
    minio_access_key=s3_access_key,
    minio_secret_key=s3_secret_key,
    minio_bucket=s3_bucket,
    secure=False
)
logger.info("Initialized S3 client")

service: StorageServiceInterface = StorageService(
    storage_service_client=storage_service_client,
)
logger.info("Initialized S3 service")

router = get_router(service)
logger.info("Initialized data router")

app.include_router(router)
logger.info("Included data router, starting up..")
